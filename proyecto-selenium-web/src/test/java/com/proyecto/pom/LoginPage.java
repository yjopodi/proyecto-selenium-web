package com.proyecto.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Base {
	
	By usernameLocator = By.id("user-name");
	By passwordLocator = By.id("password");
	
	By loginBtnLocator = By.name("login-button");
	
	By cart = By.id("shopping_cart_container");
	By message = By.className("error-button");
	
	///Aqui van todos los localizadores necesarios por ejemplo:
	/*	By userNameLocator = By.id("user-name");
		By userPassLocator = By.id("password");
		By btnLogin = By.id("login-button");	*/
	
	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void signIn() {
		if(isDiplayed(usernameLocator)) {
			teclear("standard_user", usernameLocator);
			teclear("secret_sauce", passwordLocator);
			click(loginBtnLocator);
		}else {
			System.out.println("Username textbox not present");
		}
	}
	
	public boolean isHomePageDisplayed() {
		return isDiplayed(cart);
	}
	
	public void signInNulo() {
		if(isDiplayed(usernameLocator)) {
			teclear("", usernameLocator);
			teclear("", passwordLocator);
			click(loginBtnLocator);
		}else {
			System.out.println("Username textbox not present");
		}
	}
	
	public boolean messageError() {
		return isDiplayed(message);
	}
	
	public void signInCharS() {
		if(isDiplayed(usernameLocator)) {
			teclear("h%ol&", usernameLocator);
			teclear("1#2`", passwordLocator);
			click(loginBtnLocator);
		}else {
			System.out.println("Username textbox not present");
		}
	}
}
