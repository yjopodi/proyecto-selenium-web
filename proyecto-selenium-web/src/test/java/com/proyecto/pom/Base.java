package com.proyecto.pom;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	private WebDriver driver;
	
	public Base(WebDriver driver) {
		this.driver= driver;
	}
	
	public WebDriver chromeDriverConexion() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}
	
	public WebElement findElement(By localizador) {
		return driver.findElement(localizador);
	}
	
	public List<WebElement> findElements(By localizador){
		return driver.findElements(localizador);
	}
	
	public String getText(WebElement elemento) {
		return elemento.getText();
	}
	
	public String getTextLocalizador(By localizador) {
		return driver.findElement(localizador).getText();
	}
	
	public void teclear(String inputText, By localizador) {
		driver.findElement(localizador).sendKeys(inputText);
	}
	
	public void click(By localizador) {
		driver.findElement(localizador).click();
	}
	
	public Boolean isDiplayed(By localizador) {
		try {
			return driver.findElement(localizador).isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	
	
	public void visitar(String url) {
		driver.get(url);
	}
}
