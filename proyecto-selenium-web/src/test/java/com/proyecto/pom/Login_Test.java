package com.proyecto.pom;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Login_Test {

	private WebDriver driver;
	LoginPage loginPage;
	
	@Before
	public void setUp() throws Exception{
		loginPage = new LoginPage(driver);
		driver = loginPage.chromeDriverConexion();
		loginPage.visitar("https://www.saucedemo.com/");
	}
	
	@After
	public void tearDown() throws Exception{
		driver.quit();
	}
	
	
	@Test
	public void login_test() throws InterruptedException {
		loginPage.signIn();
		Thread.sleep(2000);
		assertTrue(loginPage.isHomePageDisplayed());
	}
	
	@Test
	public void login_nulo_test() throws InterruptedException {
		loginPage.signInNulo();
		Thread.sleep(2000);
		assertTrue(loginPage.messageError());
	}

	@Test
	public void login_charS_test() throws InterruptedException {
		loginPage.signInCharS();
		Thread.sleep(2000);
		assertTrue(loginPage.messageError());
	}

}
